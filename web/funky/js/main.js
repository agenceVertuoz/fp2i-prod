;
(function ($, window, document, undefined) {

    "use strict";

    //Scroll back to top

    var offset = 300;
    var duration = 600;
    jQuery(window).on('scroll', function () {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.scroll-to-top').fadeIn(duration);
        } else {
            jQuery('.scroll-to-top').fadeOut(duration);
        }
    });

    jQuery('.scroll-to-top').on('click', function (event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    });
    

    // Carousel
    $.each($(".slides"), function () {

        var $this = $(this);
        
        var _items = $this.data('items') || 1;
        var _lazyLoad = $this.data('lazy-load') || false;
        var _loop = $this.data('loop') || false;
        var _nav = $this.data('nav') || false;
        var _dots = $this.data('dots') || false;
        var _margin = $this.data('margin') || 0;
        var _autoplay = $this.data('autoplay') || false;
        var _autoplayTimeout = $this.data('autoplay-timeout') || 5000;
        var _stagePadding = $this.data('stage-padding') || 0;
        var _slideBy = $this.data('slide-by') || 1;
        var _center = $this.data('center') || false;
        var _animateIn = $this.data('animate-in') || false;
        var _animateOut = $this.data('animate-out') || false;

        $this.find('.owl-carousel').owlCarousel({
            items: _items,
            lazyLoad: _lazyLoad,
            loop: _loop,
            nav: _nav,
            dots: _dots,
            margin: _margin,
            autoplay: _autoplay,
            autoplayTimeout: _autoplayTimeout,
            stagePadding: _stagePadding,
            slideBy: _slideBy,
            center: _center,
            animateIn : _animateIn,
            animateOut : _animateOut
        });

    });
 
    
    
    //Parallax
    $('.parallax-1').parallax("50%", 0.3);
    

    $(window).on('load', function () {

        /* Cookie Consent */
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#425cbb"
                },
                "button": {
                    "background": "#fff",
                    "text": "#237afc"
                }
            },
            "theme": "classic",
            "content": {
                "message": "En poursuivant votre navigation sur ce site, vous acceptez l’utilisation des cookies",
                "dismiss": "J'accepte",
                "link": "En savoir plus "
            }
        });

    });


})(jQuery); 