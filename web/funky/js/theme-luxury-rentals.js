;
(function ($, window, document, undefined) {

    "use strict";
    
    $('.loader').fadeIn();
    
    var viewportW = $(window).width();
    var viewportH = $(window).height();
    
    var $slidesImgArr = [];
    var $slidesTextArr = [];
    var $slider = $('.vegas-slider');
    var $sliderImgs = $slider.find('ul > li > img.imgbg');

    var _loop = $slider.data('loop') || false;
    var _delay = $slider.data('delay') || 6000;
    var _animation = $slider.data('animation') || "fade";
    var _preload = $slider.data('preload') || "false";
    var _shuffle = $slider.data('shuffle') || "false";
    var _animationDuration = $slider.data('animation-duration') || 1000;
    var _transitionDuration = $slider.data('transition-duration') || 1000;
    var i = 0;

    $.each($sliderImgs, function () {
        $slidesImgArr[i] = [];
        var $this = $(this);
        var src = $this.data('src');
        var title = $this.data('title');
        var subtitle = $this.data('subtitle');
        $slidesTextArr[i] = {
            'title' : title,
            'subtitle' : subtitle
        }
        $slidesImgArr[i] = {
            src: src
        };
        i++;
    });

    var slider = $(".vegas-slider").vegas({
        transition: "fade",
        loop: _loop,
        delay: _delay,
        preloadImage: _preload,
        shuffle: _shuffle,
        animationDuration: _animationDuration,
        transitionDuration: _transitionDuration,
        animation: [ 'kenburnsUp', 'kenburnsDown', 'kenburnsLeft', 'kenburnsRight' ],
        slides: $slidesImgArr,
        walk: function (index, slideSettings) {
            $.each($('.slide').not('.slide-' + index), function() {
                $(this).removeClass('fadeIn');
                $(this).addClass('fadeOut');
            });
            var $slide = $('.slide-' + index);
            $slide.removeClass('fadeOut');
            $slide.addClass('animated fadeIn');
        }
    });
    
    slider.on('vegasinit', function() {
        
        $('.vegas-wrapper ul').remove();
        
        var i = 0;
        
        $.each($slidesTextArr, function() {
            var slideText = this;
            var $slideText = $('<div />').addClass('slide').addClass('slide-'+i);
            var $title = $('<h2>').addClass('title').text(slideText.title);
            var $subTitle = $('<h3>').addClass('subtitle').text(slideText.subtitle);
            $slideText.append($title);
            $slideText.append($subTitle);
            $('.slides-text').append($slideText);
            i++;
        });
        
    });
    
    slider.on('vegasplay', function() {
        
        $('.loader').fadeOut();
        
        $('.logo').addClass('fadeIn');
        $('.form-container').addClass('fadeIn');
        $('.form-validate').validate();
        
    });
    
    $(window).on('resize', function () {

        viewportW = $(window).width();
        viewportH = $(window).height();
        $('.vegas-container').removeAttr('style');
        $('.vegas-container').width(viewportW);
        $('.vegas-container').height(viewportH);

    });

})(jQuery, window, document);