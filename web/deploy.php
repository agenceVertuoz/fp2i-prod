<?php

$host = $_SERVER["HTTP_HOST"];
$whitelistHost = "-argweb.vertuoz.net";
if (substr_count($host, $whitelistHost) == 0) {
    exit("Deploiement à distance non autorisé sur le host " . $host);
}

$logs = array();

$logs[] = date("d/m/Y H:i:s");

$key = filter_input(INPUT_GET, "key", FILTER_SANITIZE_STRING);


if (!is_null($key) && $key == date("d")) {
    $cmd = "git pull";
    $logs[] = $cmd;
    $ret = shell_exec($cmd);
    $logs[] = $ret;
} else {
    $logs[] = "Clé incorrecte, deploiement non effectué";
}

//Avec exec
$strTolog = implode(PHP_EOL, $logs);

print_r($strTolog);
