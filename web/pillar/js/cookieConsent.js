// cookieConsent

window.cookieconsent_options = {
        learnMore: 'En savoir plus',
        message: 'En poursuivant votre navigation sur ce site, vous acceptez l’utilisation des cookies. ',
        link: '/fr/rgpd',
        dismiss: ' J\'accepte',
        theme :  false
        //theme: '../css/cc_theme.css'
    };