;
(function ($, window, document, undefined) {

    "use strict";
    
    var $goTop;
    
    // Go top
    $goTop = $('<a />').attr('id', 'goTop').text('Haut de page'); 
    
    $goTop.on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0 
        }, 500);
        return false;
    });
    
    $goTop.appendTo('body');

//    if ($('body.homepage').length === 0) {
//        $('.nav-bar').addClass('nav--fixed');
//    }
    
    if ($('body.contact').length === 1) {
        $('.form-validate').validate();
    }
    
    $('.custom-select').niceSelect();
    
})(jQuery, window, document);

document.addEventListener('contextmenu', event => event.preventDefault());


