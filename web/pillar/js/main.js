;
(function ($, window, document, undefined) {


    "use strict";

    /**
     * Injectable Helper
     */

    $.each($('[data-injectable'), function () {

        var targetClass = $(this).data('injectable');
        var $source = $(this);
        var $target = $(this).siblings(targetClass).first();

        var $img = $source.find('img');
        var $title = $source.find('.title');
        var $link = $source.find('a');

        // Remove all links in parallax to inject slider
        if ($link.length) {
            $link.remove();
        }

        var $row = $('<div />').addClass('row');
        $row.append($target.clone());

        var html = "";
        html += "<section class=\"imagebg parallax\" data-overlay=\"3\">";
        html += "   <div class=\"background-image-holder\">" + $img.parent().html() + "</div>";
        html += "   <div class=\"container\">";
        html += "       <div class=\"row\">";
        html += "           <div class=\"col-sm-12 text-center\">" + $title.parent().html() + " </div>";
        html += "       </div>";
        html += $row.html();
        html += "   </div>";
        html += "</section>";

        // Inject slider in parallax
        $source.after(html);

        $source.remove();
        $target.remove();

    });


//    $(window).on('load', function () {
//
//        /* Cookie Consent */
//        window.cookieconsent.initialise({
//            "palette": {
//                "popup": {
//                    "background": "#425cbb"
//                },
//                "button": {
//                    "background": "#fff",
//                    "text": "#237afc"
//                }
//            },
//            "theme": "classic",
//            "content": {
//                "message": "En poursuivant votre navigation sur ce site, vous acceptez l’utilisation des cookies",
//                "dismiss": "J'accepte",
//                "link": "En savoir plus "
//            },
//
//            onStatusChange: function (status) {
//                this.element.parentNode.removeChild(this.element);
//
//            }
//        });
//
//    });

})(jQuery, window, document);




function searchAnnonces() {
    $('#chargement').show();
    var html = "";
    var reference = $('#referenceId').val();
    var section = $('#sectionAnnonce').attr('class');
    var vente = $('#idSearch0').val();
    var typeDeBiens = $('#idSearch1').val();
    var surface = $('#idSearch3').val();
    var secteur = $('#idSearch2').val();
    var pageTitle = $('#pageTitle').html();
    var str = "";
    var language = $('#language').text();
    //console.log(reference,section,vente,typeDeBiens,surface,secteur,pageTitle);
    //remplacelment des - de bouches-du-rhone
    //secteur=secteur.replace(new RegExp("-", 'g'),' ');
    //var str = form.serialize();

    $.ajax({
        
        url: "/render-annonces?tplName=vertical-blog-cards-item&collectionTpl=vertical-blog-cards&language=" + language + "&areaName=page-personnalisee&order=id&direction=desc" + "&reference=" + reference + "&criteria0=" + vente + "&criteria1=" + typeDeBiens + "&criteria61=" + surface + "&criteria3=" + secteur,
        type: 'GET',
        //data: str,
        complete: function () {
            //display_requestLoader('none');
        },
        success: function (response) {
            $('#ajaxAnnonces').empty().html(response);
            $('#chargement').hide();
            if (typeof vente != "undefined") {
                str += vente + " ";
            }
            if (typeof typeDeBiens != "undefined") {
                str += typeDeBiens + " ";
            }
            if (typeof secteur != "undefined") {
                str += secteur + " ";
            }
            $("#pageTitle").text(str);
            //remplacement de la class au cas ou recherche apres flash info 
            $('#sectionAnnonce').addClass('annonce');
            //console.log( secteur,vente,typeDeBiens,surface );
            loueVendu();
            urlSpaceReplace();
        },
        error: function (response) {
            $('#ajaxAnnonces').empty().html(response);

            //console.log( response );
        }

    });

    //console.log(section);
    //$("#ajaxAnnonces").empty().html(html);
}

//lie les images mignatures avec le carousel

function btnJump(id) {
    //$('#btnJump').click(function(){
    $('.owl-carousel').trigger('to.owl.carousel', [id, 2, true]);
    //console.log('t');
    //});

}
;

//recupere la valeur loué ou vendu et l'affiche dans .card__image ::before
function loueVendu() {
    $('.card.card-4 .card__image2').each(function () {
        var id = $(this).attr('id');
        var content = $('.vente' + id).html();
        $(this).attr('data-content', content);
    });
}
;

loueVendu();

//remplace les espaces des url de liens par des -
function urlSpaceReplace() {
    $(".hrefUrl").each(function () {
        var id = $(this).attr('id');
        var url = $("." + id).attr('href');
        url = url.replace(new RegExp(" ", 'g'), '-');
        $("." + id).attr('href', url);
    });
}
;

urlSpaceReplace();

//map 

function initMap() {
    var lat = parseFloat($('#lat').html());
    var lng = parseFloat($('#lng').html());
    var title = $('#title').html();
    var tplMapName = $('#tplMapName').html();
    //console.log(lat,lng,title,tplMapName);
    var position = {lat: lat, lng: lng};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: position
    });
     
        
    var infowindow = new google.maps.InfoWindow({
        content: title
    });
    if (tplMapName == 'contact') {
        var marker = new google.maps.Marker({
            position: position,
            map: map
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
    } else {
        var circle = new google.maps.Circle({
            strokeColor: '#bc151a',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#bc151a',
            fillOpacity: 0.35,
            map: map,
            center: position,
            radius: 1000
        });
        circle.addListener('click', function () {
            infowindow.open(map, circle);
        });
    }
}




function refuserToucheEntree(event)
{
    // Compatibilité IE / Firefox
    if (!event && window.event) {
       // event = window.event;
    }
    // IE
    if (event.keyCode == 13) {
        event.returnValue = true;
        event.cancelBubble = false;
        //searchAnnonces();
    }
    // DOM
    if (event.which == 13) {
        event.preventDefault();
        event.stopPropagation();
        //searchAnnonces();
    }
}

$("#referenceId").on('keyup', function (e) {
    if (e.keyCode == 13) {
        searchAnnonces();
    }
});

$(document).ready(function () {
    
    //Si on est sur une page annonce
    if($(".cont-annonce").length > 0) {
        //dpe 
        var classDpe = $('#classDpe').html();

        if (classDpe.length > 0) {
            $('#pointerDpe').addClass('dpe' + classDpe);
        } else {
            $('#dpeEnCours').addClass('dpeEnCours');
            $('#dpeEnCoursTitre').removeClass('dpeEnCoursTitre');
        }

        //contactAnnonce
        var ref = $('#reference').html();
        str1 = "Bonjour,\n";
        str2 = "Je souhaiterais être contacté au sujet de l'offre n°" + ref + "\n";
        str3 = "Je souhaiterais recevoir plus d'informations au sujet de l'offre n°" + ref + "\n";
        str4 = "Cordialement.";
        $('#form_message').val(str1 + str2 + str3 + str4);

        $("#plusDinformation").click(function () {

            if ($(this).is(":checked"))
            {
                if ($('#etreRappele').is(':checked')) {
                    $('#form_message').val(str1 + str2 + str3 + str4);
                } else {
                    $('#form_message').val(str1 + str3 + str4);
                }
            } else {
                if ($('#etreRappele').is(':checked')) {
                    $('#form_message').val(str1 + str2 + str4);
                } else {
                    $('#form_message').val(str1 + str4);
                }
            }
        });

        $("#etreRappele").click(function () {

            if ($(this).is(":checked"))
            {
                if ($('#plusDinformation').is(':checked')) {
                    $('#form_message').val(str1 + str2 + str3 + str4);
                } else {
                    $('#form_message').val(str1 + str2 + str4);
                }

            } else {
                if ($('#plusDinformation').is(':checked')) {
                    $('#form_message').val(str1 + str3 + str4);
                } else {
                    $('#form_message').val(str1 + str4);
                }
            }
        });
    }

});




