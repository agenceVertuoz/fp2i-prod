<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('render-event-collection/{viewType?}', function (Request $request) use ($app) {

    $viewType = $request->get('viewType');
    $instanceId = $request->get('instanceId');
    $tplName = $request->get('tplName', 'block');
    
    $apiClient = $app["api-client"];
    $twigData = array();
    $noElementText = "Aucun élement";
    $collectionTpl = "carousel";
    
    if ($request->get('collectionTpl')) {
        $collectionTpl = $request->get('collectionTpl');
    }

    if ($request->get('noElementText')) {
        $noElementText = $request->get('noElementText');
    }

    $viewPath = "components/content-collection/$collectionTpl.html.twig";

    try {

        $params = [];
        $eventHelper = new \Vertuoz\Utils\EventHelper($apiClient);
        $events = $eventHelper->listEvents($params);
    
        $twigData["events"] = $events;
        $twigData["tplName"] = $tplName;
        $twigData["no_element_text"] = $noElementText;
                
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        exit($e->getMessage());
    }

    return $app['twig']->render($viewPath, $twigData);

})->bind('render-event-collection');

$app->get('render-weather/{tplName}', function(Request $request) use ($app) {
    
    $areaName = $request->get('areaName');
    $tplName = $request->get('tplName');
    
    $apiUrl = $app['config']['app']['openweathermap']['apiUrl'];
    $appId = $app['config']['app']['openweathermap']['appId'];
    $location = $app['config']['app']['openweathermap']['location'];
    
    $cacheFile = __DIR__ . '/../var/weather/response.txt';

    $weatherHelper = new \Vertuoz\Utils\OpenWeatherMapHelper($apiUrl, $appId, $location, $cacheFile);

    $twigData = array();
    $data = $weatherHelper->getData();
    $twigData['icon'] = $data['icon'];
    $twigData['tempMin'] = $data['tempMin'];
    $twigData['tempMax'] = $data['tempMax'];
    
    $viewPath = "components/weather/" . $tplName . ".html.twig";
 
    return $app['twig']->render($viewPath, $twigData);
    
})->bind('render-weather');


$app->get('/{_locale}/{pageUrl}', function (Request $request) use ($app) {
    
    $locale = $request->get('_locale');
    //die('ici');
    $arrIndex = array(
        "categorie" => 0,
        "page-personnalisee" => 1,
        "page-contenu" => 2,
        "actualite" => 3,
        "bloc" => 4,
        "catalogue" => 5,
        "description" => 6
    );
    
    try {
        $params = $request->query->all();

        $readMoreLink = $request->get('pageUrl');
        
        $apiClient = $app["api-client"];
        //die($readMoreLink);
        // Check si un slug db existe
        $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
        $content = $contentHelper->getOneByReadMoreLinkAndAreaNameNotIn($readMoreLink,$locale, array(
            "bloc"
        ));
       //die(var_dump($content));

        if (is_null($content)) {
            // Check si get by id

            $content = $contentHelper->get($readMoreLink);

            if (is_null($content)) {
                throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
            }
        }
        //dump($content);////////////////////////////////

        $twigData = array();
        $viewType = "full";
        $contentType = "component/content";
        $link = $request->get('link');

        $params = $request->get('params');

        $areaName = $content['areaName'];
        //$extrafield = $content['extrafields']['language1'][$arrIndex[$areaName]]['value'];
        $extrafield = "page-contenu-fp2i";

        $viewPath = $areaName . '/' . $extrafield;
        //dump($viewPath);
        $app['active_page'] = '/'.$readMoreLink;
        
        $twigData["language"] = $locale;
        $twigData["content"] = $content;
        $twigData["id"] = $content["id"];
        $twigData["link"] = $link;
        $twigData["analytics_account"] = $app['config']['app']['analytics_account'];
        if (is_array($params)) {
            $twigData['content']['params'] = $params;
        }

        //dump($app);
        $tplPath = 'views/'.$viewPath;
 
        $url = "/$readMoreLink";
        $tplOverride = Vertuoz\Utils\TemplateHelper::getTemplateOverrideByUrl($url, $app['config']['pages']);
        if (!is_null($tplOverride)) {
            $tplPath = $tplOverride;
        }
        
        try {
             return $app['twig']->render($tplPath . '.html.twig', $twigData);
            
        } catch (Twig_Error_Loader $twigErrorLoader) {
            throw new Vertuoz\Exception\ArgWeb("Erreur lors du chargement du gabarit ".basename($tplPath)." introuvable : ".$twigErrorLoader->getMessage(),$twigErrorLoader->getCode());
        }
       
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
    }
})->bind('urlcatcher');


$app->get('/render-area/{viewType}/{tplName}/{areaName}', function (Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'une zone
             * @param string viewType - Type de vue du contenu (full, block, inline, ...)
             * @param string areaName - Nom de la zone (z1, z2, ...)
             * @return twig template
             */
            $apiClient = $app["api-client"];
            $twigData = array();

            $areaName = $request->get('areaName');
            $viewType = $request->get('viewType');
            $tplName = $request->get('tplName');

            $viewPath = "area/" . $areaName . ".html.twig";


            try {
                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
                $contents = $contentHelper->getAllByArea($areaName);
                $twigData["contents"] = $contents;
                $twigData["viewType"] = $viewType;
                $twigData["tplName"] = $tplName;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }


            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-area');


$app->get('/render-content-link/{tplName}', function (Request $request) use ($app) {

            $twigData = array();
            $object = $request->get('object');
            $tplName = $request->get('tplName');
            $linkText = $request->get('linkText');
            $title = $object["title"];
            $language = $request->get('language');
            
            if (!is_null($linkText)) {
                $title = $linkText;
            }
            $href = '';

            $viewPath = "component/content/" . $tplName . ".html.twig";

            if (strlen($object['readMoreLink']) > 0) {
                $href = $object['readMoreLink'];
            } else {
                $href = "/" . $object['id'];
            }

            $twigData["title"] = $title;
            $twigData["href"] = $href;
            $twigData["content"] = $object;
            $twigData["language"] = $language;

            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-content-link');


$app->get('/render-breadcrumb/{viewType}/{tplName}', function (Request $request) use ($app) {

            $apiClient = $app["api-client"];
            $twigData = array();
            $viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            $object = $request->get('object');

            $viewPath = "component/breadcrumb/" . $viewType . "/" . $tplName . ".html.twig";

            $twigData['title'] = $object['title'];

            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-breadcrumb');
        
        
$app->get('/render-event-object/{viewType?}/{tplName}', function (Request $request) use ($app) {

            $twigData = array();

            $event = $request->get('eventObject');
            $viewType = $request->get('viewType', null);
            $tplName = $request->get('tplName');

            $viewPath = "components/event/" . $tplName . ".html.twig";

            try {
                $twigData["event"] = $event;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }
            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-event-object');


$app->get('/render-content-object/{viewType?}/{tplName}', function ($viewType = null, Request $request) use ($app) {

            $twigData = array();

            $content = $request->get('contentObject');
            $viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            $language = $request->get('language');
            $params = $request->get('params');
            if(isset($content['fields'])){
                if($content['fields']>0){

                   $content['url'] = rewriteStringToUrl($content['fields'][0][0]['valeur']."-".$content['fields'][0][1]['valeur']."-".$content['fields'][0][2]['valeur']."-".$content['id']);
                   //dump($content); 
                }
            }
            
            $viewPath = "components/content/" . $viewType . "/" . $tplName . ".html.twig";

            try {
                $twigData["content"] = $content;
                $twigData["id"] = $content["id"];
                $twigData["language"] = $language;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }

            if (is_array($params)) {
                $twigData['content']['params'] = $params;
            }
            
            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-content-object');


$app->get('/render-content/{viewType?}/{tplName}/{contentCode}', function ($viewType = null, Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'un contenu
             * @param string viewType - Type de vue du contenu (full, block, inline, ...)
             * @param string contentCode - Code du contenu (C1, C2, ...)
             * @return twig template
             */
            $apiClient = $app["api-client"];
            $twigData = array();
            $language = $request->get('language');

            $contentCode = $request->get('contentCode');
            //$viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            $link = $request->get('link');

            $params = $request->get('params');
            //dump($params);
            $viewPath = "components/content/" . $tplName . ".html.twig";

            try {
                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
                $content = $contentHelper->getOneByCode($contentCode,$language);

                if (is_null($content)) {
                    $viewPath = "views/errors/content-notfound.html.twig";
                    return $app['twig']->render($viewPath, array("errorMessage" => "Code $contentCode non trouvé"));
                }
                
                $twigData["language"] = $language;
                $twigData["content"] = $content;
                $twigData["id"] = $content["id"];
                $twigData["link"] = $link;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }

            if (is_array($params)) {
                $twigData['content']['params'] = $params;
            }
            //dump($language);
            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-content');


$app->get('/render-slider/{tplName}/{sliderName}/{viewType}', function ($viewType = null, Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'un slider
             * @param string viewType - Type de vue du slider (full)
             * @param string sliderName - Nom du slider (SW1, SW2, ...)
             * @return twig template
             */
            $apiClient = $app["api-client"];
            $language = $request->get('language');

            $twigData = array();
            $params = $request->query->all();

            $sliderName = $request->get('sliderName');
            //$viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            
            $viewPath = "components/slider/" . $tplName . ".html.twig";
            
            try {
                $sliderHelper = new \Vertuoz\Utils\SliderHelper($apiClient);
                $slider = $sliderHelper->getOneByName($sliderName, $language);
                
                if (is_null($slider)) {
                    $viewPath = "views/errors/content-notfound.html.twig";
                    return $app['twig']->render($viewPath, array("errorMessage" => "Slider $sliderName non trouvé"));
                }
                
                $slider = $sliderHelper->get($slider["id"],$language);
                $slides = [];

                foreach ($slider['slides'] as $key => $slide) {
                    $imgUrl = Vertuoz\Utils\Slideshow::getSlidePictureFullUrl($app['config']['app']['cdn']['base_url'], $app['config']['app']['id'], $app['config']['pages']['all']['slider'], $slide['file']);
                    $slider['slides'][$key]['file'] = $imgUrl;
                }

                $twigData["slider"] = $slider;
                $twigData["name"] = $sliderName;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }

            //dump($twigData);
            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-slider');
        
$app->get('/render-slider-by-id/{tplName}/{sliderId}/{viewType}', function ($viewType = null, Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'un slider
             * @param string viewType - Type de vue du slider (full)
             * @param string sliderName - Nom du slider (SW1, SW2, ...)
             * @return twig template
             */
            $apiClient = $app["api-client"];

            $twigData = array();
            $params = $request->query->all();

            $sliderId = $request->get('sliderId');
            //$viewType = $request->get('viewType');
            $tplName = $request->get('tplName');

            $viewPath = "components/slider/" . $tplName . ".html.twig";
            
            try {
                $sliderHelper = new \Vertuoz\Utils\SliderHelper($apiClient);

                $slider = $sliderHelper->get($sliderId);
                $slides = [];

                foreach ($slider['slides'] as $key => $slide) {
                    $imgUrl = Vertuoz\Utils\Slideshow::getSlidePictureFullUrl($app['config']['app']['cdn']['base_url'], $app['config']['app']['id'], $app['config']['pages']['all']['slider'], $slide['file']);
                    $slider['slides'][$key]['file'] = $imgUrl;
                }

                $twigData["slider"] = $slider;
                $twigData["name"]   = $slider['name'];
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }


            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-slider-by-id');


$app->get('/render-partial/{name}', function (Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'un partial
             * @param string name - Nom du fichier
             * @return twig template
             */
            $name = $request->get('name');
            $viewPath = "partials/" . $name . ".html.twig";

            $pages = $app['config']['pages'];
            $twigData['pages'] = $pages;

            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-partial');



$app->get('/render-organise/{viewType?}/{tplName}/{areaName}', function ($viewType = null, Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'un menu organise
             * @param string tplName - nom du template à utiliser
             * @param string areaName - zone de contenu du menu
             * @param string viewType - Type de vue du menu
             * @return twig template
             */
            $apiClient = $app["api-client"];
            $twigData = array();
            $language = $request->get('language');
            //$language = 'fr';
            $params = $request->query->all();
            //$viewType = $request->get('viewType');
            $areaName = $request->get('areaName');
            $tplName = $request->get('tplName');
            $viewPath = "components/organise/" . $tplName . ".html.twig";
           
            try {

                $organiseHelper = new \Vertuoz\Utils\OrganiseHelper($apiClient);
                $organise = $organiseHelper->getOneByArea($areaName,$language);

                if (is_null($organise)) {
                    $viewPath = "views/errors/content-notfound.html.twig";
                    return $app['twig']->render($viewPath, array("errorMessage" => "Menu zone $areaName non trouvé"));
                }
                
          
                        
                $twigData["language"] = $language;
                $twigData["organise"] = $organise;
                $twigData["organise"]['options'] = [];
                $twigData["id"] = $organise["id"];
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return $e->getMessage();
            }
            //dump($language);
            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-organise');



$app->get('/render-content-collection/{viewType?}/{tplName}/{areaName?}/{limit?}/{order}/{direction}/[extrafield]', function ($viewType = null, Request $request) use ($app) {

            /**
             * @desc Méthode de rendu d'une zone
             * @param string viewType - Type de vue du contenu (full, block, inline, ...)
             * @param string tplName - nom du template à utiliser
             * @param string areaName - Nom de la zone (z1, z2, ...)
             * @param string limit - Entier pour limiter le nombre de résultats
             * @param string order - Champ sur lequel on veut trier
             * @param string direction - Tri "asc" ou "desc"
             * @param string language -  "fr" ou "en"
             * @param array extrafield - Valeurs des tri par extrafields
             * @return twig template
             */
            $apiClient = $app["api-client"];
            $twigData = array();
            $language = $request->get('language');
            $collectionTpl = "liste-d";
            if ($request->get('collectionTpl')) {
                $collectionTpl = $request->get('collectionTpl');
            }
            $areaName = $request->get('areaName');
            //$viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            $code = $request->get('code');

            $limit = $request->get('limit');
            $order = $request->get('order');
            $direction = $request->get('direction');
            $fieldFilters = $request->get('extrafield');

            $noElementText = "Aucun élement";

            if (!is_null($request->get('noElementText'))) {
                $noElementText = $request->get('noElementText');
            }

            $viewPath = "components/content-collection/$collectionTpl.html.twig";

            try {
                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);

                $params = array(
                    "areaName" => $areaName,
                    "limit" => $limit,
                    "order" => $order,
                    "code" => $code,
                    "direction" => $direction,
                    "language" => $language,
                    "extrafield" => $fieldFilters
                );

                $contents = $contentHelper->listContents($params);
        
                if (empty($contents)) {
                    $viewPath = "views/errors/content-notfound.html.twig";
                    return $app['twig']->render($viewPath, array("errorMessage" => "Contenus $code non trouvés"));
                }
                $twigData["language"] = $language;
                $twigData["contents"] = $contents;
                $twigData["viewType"] = $viewType;
                $twigData["tplName"] = $tplName;
                $twigData["areaName"] = $areaName;
                $twigData["no_element_text"] = $noElementText;
                
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                
            }

           //dump($contents);
            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-content-collection');

$app->get('/render-extrafield/{viewType}/{tplName}/{index}', function (Request $request) use ($app) {

            $apiClient = $app["api-client"];
            $twigData = array();

            $index = $request->get('index');
            $viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            $urlPrefix = $request->get('urlPrefix', '');

            $viewPath = "components/extrafield/$viewType/$tplName.html.twig";
            try {
                $contentHelper = new \Vertuoz\Utils\ExtrafieldHelper($apiClient);
                $rubrique = $contentHelper->getByIndex($index);

                $twigData["rubrique"] = $rubrique;
                $twigData["viewType"] = $viewType;
                $twigData["tplName"] = $tplName;
                $twigData['urlPrefix'] = $urlPrefix;
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                //   exit($e->getMessage());
            }


            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-extrafield');


$app->get('/render-extrafield-link/{tplName}', function (Request $request) use ($app) {

            $object = $request->get('object');
            $tplName = $request->get('tplName');
            $href = '';

            $viewPath = "components/extrafield/link/" . $tplName . ".html.twig";

            if (strlen($object['slug']) > 0) {
                $href = '/actualites/' . $object['slug'];
            } else {
                $href = '/' . $object['id'];
            }

            $twigData["href"] = $href;
            $twigData["content"] = $object;

            return $app['twig']->render($viewPath, $twigData);
        })
        ->bind('render-extrafield-link');
        
        

/**
 * Erreur
 */
$app->error(function (\Exception $e, Request $request, $code) use ($app) {

    $message = $e->getMessage();
    
    if ($app['debug']) {
        return;
    }
    
    if($e instanceof Vertuoz\Exception\ArgWeb)
    {
        $code = "argweb";
        $message = $e->getMessage();
    }

    $twigData = array();
    $twigData["analytics_account"] = $app['config']['app']['analytics_account'];
    $twigData["code"] = $code;
    $twigData["message"] = $message;
    
    // 404.html, or 40x.html, or 4xx.html, or error.html
 
    $templates = array(
        $app['theme'] . '/views/' . 'errors/' . $code . '.html.twig',
        $app['theme'] . '/views/' . 'errors/' . substr($code, 0, 2) . 'x.html.twig',
        $app['theme'] . '/views/' . 'errors/' . substr($code, 0, 1) . 'xx.html.twig',
        $app['theme'] . '/views/' . 'errors/default.html.twig',
    );

    return $app['twig']->resolveTemplate($templates)->render($twigData);
});

   function rewriteStringToUrl($string, $addHtml = true, $minLength=2) {

        $convertedCharacters = array(
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a',
            'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O',
            'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
            'é' => 'e', 'è' => 'e', 'ê' => 'e', 'ë' => 'e',
            'Ç' => 'C', 'ç' => 'c',
            'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
            'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
            'ÿ' => 'y',
            'Ñ' => 'N', 'ñ' => 'n'
        );

        $text = strtr($string, $convertedCharacters);
        $text = mb_strtolower($text, 'utf-8');
        $text = preg_replace('#[^a-z0-9\-&]#', '-', $text);
        $text = preg_replace('/--/U', '-', $text);

        $return = array();
        $text = explode('-', $text);

        foreach ($text as $word) {
            if (mb_strlen($word, 'utf-8') >= $minLength || $word == "&")
                $return[] = $word;
        }

        if ($addHtml)
            return implode('-', $return) . ".html";
        else
            return implode('-', $return);
    }      