<?php

use Form\AnnonceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints as Assert;
use ReCaptcha\ReCaptcha;
use Spipu\Html2Pdf\Html2Pdf;

$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\ValidatorServiceProvider());


/**
 * Accueil
 */
$app->get('/', function (Request $request ) use ($app) {

            if (!$app['session']->get('language')) {
                $app['session']->set('language', 'fr');
            }
            $language = $app['session']->get('language');
            return $app->redirect('/' . $language . '/');
        })
        ->bind('home');

$app->get('/{_locale}/', function (Request $request ) use ($app) {
            $locale = $request->get('_locale');
            //langue

            $language = $locale;

            if (!$app['session']->get('language')) {
                $app['session']->set('language', $locale);
            }

            $language = $app['session']->get('language');

            $apiClient = $app["api-client"];
            $twigData = array();
            $twigData["content"] = [];
            $twigData["content"]['title'] = 'Accueil';
            $pages = $app['config']['pages'];

            try {

                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
                $twigData["analytics_account"] = $app['config']['app']['analytics_account'];
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                exit($e->getMessage());
            }

            $app['active_page'] = '/';

            $defaultTpl = "index";
            $tplPath = $app['config']['app']['theme'] . '/views/' . $defaultTpl;
            $url = "/";
            $tplOverride = Vertuoz\Utils\TemplateHelper::getTemplateOverrideByUrl($url, $app['config']['pages']);

            if (!is_null($tplOverride)) {
                $tplPath = $tplOverride;
            }
            //dump($locale);
            $twigData["language"] = $language;
            return $app['twig']->render($tplPath . '.html.twig', $twigData);
        })
        ->bind('accueil');


/**
 * Langue
 * appelé de nav-fp2i.html.twig (a changer par methode language locale)
 */
$app->get('/language/{language}', function (Request $request) use ($app) {

            $language = $request->get('language');
            $app['session']->set('language', $language);

            return $app->redirect('/' . $language . '/');
        })
        ->bind('language');

/**
 * Actualités
 */
//$app->get('/actualites/{rubriqueSlug}', function ($rubriqueSlug = null) use ($app) {
//
//            $apiClient = $app["api-client"];
//            $twigData = array();
//
//            $extrafieldHelper = new \Vertuoz\Utils\ExtrafieldHelper($apiClient);
//            $rubrique = $extrafieldHelper->getByIndex(0);
//            $currentRubrique = null;
//
//            foreach ($rubrique["options"] as $option) {
//
//                if ($option["slug"] == $rubriqueSlug) {
//                    $currentRubrique = $option;
//                    break;
//                }
//            }
//
//            try {
//                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
//                $content = $contentHelper->getOneByCode("actualites");
//
//                $twigData["content"] = $content;
//            } catch (\GuzzleHttp\Exception\ClientException $e) {
//                //   exit($e->getMessage());
//            }
//
//            $twigData['current_rubrique'] = $currentRubrique;
//            $app['active_page'] = "/actualites";
//            $twigData["analytics_account"] = $app['config']['app']['analytics_account'];
//
//
//            $defaultTpl = "views/actualites";
//            $tplPath = $app['config']['app']['theme'] . '/' . $defaultTpl;
//
//            $url = "/actualites";
//            if (!is_null($rubriqueSlug)) {
//                $url = "/actualites/$rubriqueSlug";
//            }
//
//            $tplOverride = Vertuoz\Utils\TemplateHelper::getTemplateOverrideByUrl($url, $app['config']['pages']);
//            if (!is_null($tplOverride)) {
//                $tplPath = $tplOverride;
//            }
//            return $app['twig']->render($tplPath . '.html.twig', $twigData);
//        })
//        ->value('rubriqueSlug', null)->bind('actualites');
//


/**
 * Contact
 */
$app->match('/{_locale}/contact', function (Request $request) use ($app) {
            //dump($request);

            $language = $request->get('_locale');
            //die($language);
            $apiClient = $app["api-client"];
            $twigData = array();
            $id = $request->get('id');
            $criteria0 = $request->get('criteria0');
            $criteria1 = $request->get('criteria1');
            $criteria3 = $request->get('criteria3');
            $twigData['criteria0'] = $criteria0;
            $twigData['criteria1'] = $criteria1;
            $twigData['criteria3'] = $criteria3;
            $twigData['id'] = $id;
            $twigData['language'] = $language;
            //dump($id);
            $app['active_page'] = '/contact';
//
//            try {
//                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
//                $content = $contentHelper->getOneByCode("contact",$language);
//                $twigData['content'] = $content;
//                $twigData['content']['title'] = "Contact";
//                $twigData["analytics_account"] = $app['config']['app']['analytics_account'];
//            } catch (\GuzzleHttp\Exception\ClientException $e) {
//                exit($e->getMessage());
//            }
            $labelMessage = 'Votre message concerne *';
            $labelNom = 'votre nom *';
            $labelPrenom = 'prénom';
            $labelM = 'Votre message ';
            $labelMail = 'votre adresse e-mail *';
            $labelBtn = 'Envoyer';
            $labelRgpd = "J'accepte l'enregistrement de mes données personnelles pour le traitement de ma demande.";
            $labelOffre = 'Si offre précisez la référence';
            $labelSoc = 'Société';
            $labelCiv = 'Civilité *';
            $labelTel = 'Téléphone *';
            $labelCp = 'CP/VILLE';

            if ($language != 'fr') {
                $labelMessage = 'Your message concerns *';
                $labelNom = 'your name *';
                $labelMail = 'your e-mail *';
                $labelBtn = 'Send';
                $labelRgpd = 'you accept the conditions';
                $labelOffre = 'If offer specify the reference';
                $labelSoc = 'Society';
                $labelCiv = 'Civility *';
                $labelPrenom = 'first name';
                $labelM = 'your message ';
                $labelTel = 'phone *';
                $labelCp = 'PC/CITY';
            }

            $isSpam = false;
            $isSpamTime = 5;
            $data = array();

            $form = $app['form.factory']->createBuilder(FormType::class, $data)
                    ->add('name', TextType::class, array(
                        'label' => $labelNom,
                        'required' => true,
                        'constraints' => array(
                            new Assert\NotBlank(array(
                                'message' => 'Veuillez indiquer votre nom',
                                    ))
                        )
                    ))
                    ->add('email', EmailType::class, array(
                        'label' => $labelMail,
                        'required' => false,
                        'constraints' => array(
                            new Assert\Email(array(
                                'message' => 'Veuillez indiquer une adresse e-mail valide',
                                'checkMX' => false,
                                    )
                            ))
                    ))
                    ->add('message', TextareaType::class, array(
                        'label' => $labelM,
                        'required' => true,
                        'constraints' => array(
                            new Assert\NotBlank(array(
                                'message' => 'Veuillez indiquer un message',
                                    )),
                        ),
                    ))->add('prenom', TextType::class, array(
                        'label' => $labelPrenom,
                        'required' => false,
                    ))
                    ->add('phone', TextType::class, array(
                        'label' => $labelTel,
                        'required' => true,
                    ))
                    ->add('ville', TextType::class, array(
                        'label' => $labelCp,
                        'required' => false,
                    ))
                    ->add('raison', ChoiceType::class, array(
                        'label' => $labelMessage,
                        'required' => true,
                        'choices' => array(
                            '- Choisissez -' => null,
                            'une recherche' => 'recherche',
                            'une offre' => 'offre',
                        ),
                        'attr' => array('class' => 'nice-select custom-select custom-select-lg')
                    ))
                    ->add('offre', TextType::class, array(
                        'label' => $labelOffre,
                        'required' => false,
                    ))
                    ->add('societe', TextType::class, array(
                        'label' => $labelSoc,
                        'required' => false,
                    ))
                    ->add('civilite', ChoiceType::class, array(
                        'label' => $labelCiv,
                        'required' => true,
                        'choices' => array(
                            '- Choisissez -' => null,
                            'Mr' => 'Mr',
                            'Mme' => 'Mme',
                            'Mlle' => 'Mlle',
                            'Autre' => 'Autre',
                        ),
                        'attr' => array('class' => 'nice-select custom-select custom-select-lg')
                    ))
                    ->add('rgpd', CheckboxType::class, array(
                        'label' => $labelRgpd,
                        'required' => true,
                        'label_attr' => array(
                            'class' => 'checkbox-inline'
                        )
                    ))
                    // Anti spam
                    ->add('fax', HiddenType::class, array(
                        'label' => "Votre fax",
                        'required' => false,
                        'attr' => array(
                            'tabindex' => '-1',
                            'autocomplete' => 'off'
                        )
                    ))
                    // Anti spam
                    ->add('portable', HiddenType::class, array(
                        'label' => "Votre portable",
                        'data' => base64_encode(time()),
                        'required' => true,
                        'attr' => array(
                            'tabindex' => '-1',
                            'autocomplete' => 'off'
                        )
                    ))
                    ->add('submit', SubmitType::class, [
                        'label' => $labelBtn,
                        'attr' => array('class' => 'btn btn--sm btn--primary btnContact')
                    ])
                    ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $data = $form->getData();
                //die(var_dump($data));

                $title = $app['config']['app']['contact']['email_title'];
                $from = $app['config']['app']['contact']['from'];
                $to = $app['config']['app']['contact']['to'];

                //$data = $form->getData();

                $fax = $data['fax'];
                $portable = base64_decode($data['portable']);
                $name = $data['name'];
                $message = $data['message'];

                $recaptcha = new ReCaptcha('6LcFo4EUAAAAACfk-HLlWpUMQ3tN6L0JoBjidIrf');
                $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());

                if (!$resp->isSuccess()) {
                    // Do something if the submit wasn't valid ! Use the message to show something
                    foreach ($resp->getErrorCodes() as $code) {
                        //die ('capt');
                        $message .= $code;
                    }
                } else {

                    // Anti spam
                    if (strlen($fax) > 0) {
                        // Le champ fax a été rempli : SPAM
                        $isSpam = true;
                    } elseif ($portable + $isSpamTime > time()) {
                        // Le formulaire a été soumis en moins de 5 secondes : SPAM
                        $isSpam = true;
                    } else {

                        $body = "";
                        $body .= "Sujet : " . $data['raison'] . "<br />";
                        $body .= "Offre : " . $data['offre'] . "<br />";
                        $body .= "Société : " . $data['societe'] . "<br />";
                        $body .= "Civilite : " . $data['civilite'] . "<br />";
                        $body .= "Nom : " . $name . "<br />";
                        $body .= "Prenom : " . $data['prenom'] . "<br />";
                        $body .= "Téléphone : " . $data['phone'] . "<br />";
                        $body .= "Ville : " . $data['ville'] . "<br />";
                        $body .= "Email : " . $data['email'] . "<br />";
                        $body .= "<br />";
                        $body .= nl2br($message);


                        //THOMAS - ENVOI DU MAIL
                        $transport = (new \Swift_SmtpTransport("91.121.22.221", 25))
                        ->setUsername("ne_pas_repondre@elocms.com")
                        ->setPassword("!!5p1r1706");

                        // Create the Mailer using your created Transport
                        $mailer = new \Swift_Mailer($transport);

                        // Create a message
                        $message = (new Swift_Message("Nouveau contact sur le site"))
                          ->setFrom(["ne_pas_repondre@elocms.com" => 'Fp2i Entreprises'])
                          ->setTo("contact@fp2i-entreprises.com")      
                          ->setBcc("thomas@vertuoz.fr")
                          ->setBody($body,'text/html');

                        // Send the message
                        $mailer->send($message);

//                        $mail = \Swift_Message::newInstance()
//                                ->setSubject($title)
//                                ->setFrom(array($from))
//                                ->setTo(array($to))
//                                ->setBody($body, 'text/html');
//
//                        $app['mailer']->send($mail);

                        $twigData['sended'] = true;
                        $twigData["language"] = $language;

                        if ($id > 0) {
                            return $app->redirect('/' . $language . '/annonce-' . $id . '.html?' . http_build_query(array('messageForm' => 1)));
                        }

                        return $app['twig']->render('views/contact.html.twig', $twigData);
                    }
                }
            }

            $twigData["language"] = $language;
            $twigData['spam'] = $isSpam;
            $twigData['form'] = $form->createView();

            if ($request->get('collectionTpl')) {
                $collectionTpl = $request->get('collectionTpl');
            } else {
                $collectionTpl = "contact";
            }
            $tplPath = $app['config']['app']['theme'] . '/views/' . $collectionTpl;
            $url = "/contact";

            $tplOverride = Vertuoz\Utils\TemplateHelper::getTemplateOverrideByUrl($url, $app['config']['pages']);
            if (!is_null($tplOverride)) {
                $tplPath = $tplOverride;
            }
            if ($form->isSubmitted() && $id > 0) {
                return $app->redirect('/' . $language . '/annonce-' . $app['slugify']->slugify($criteria0) . '-' . $app['slugify']->slugify($criteria1) . '-' . $app['slugify']->slugify($criteria3) . '-' . $id . '.html?' . http_build_query(array('messageForm' => 2)));
            }

            return $app['twig']->render($tplPath . '.html.twig', $twigData);
        })
        ->bind('contact');

$app->match('/render-annonces', function ($viewType = null, Request $request) use ($app) {
            //dump($request);
            /**
             * @desc Méthode de rendu d'une zone
             * @param string viewType - Type de vue du contenu (full, block, inline, ...)
             * @param string tplName - nom du template à utiliser
             * @param string areaName - Nom de la zone (z1, z2, ...)
             * @param string limit - Entier pour limiter le nombre de résultats
             * @param string order - Champ sur lequel on veut trier
             * @param string direction - Tri "asc" ou "desc"
             * @param string language -  "fr" ou "en"
             * @param array extrafield - Valeurs des tri par extrafields
             * @return twig template
             */
            $apiClient = $app["api-client"];
            $twigData = array();
            $language = $request->get('language');
            $collectionTpl = "fiche-produit-fp2i";

            if ($request->get('collectionTpl')) {
                $collectionTpl = $request->get('collectionTpl');
            }
            $areaName = $request->get('areaName');
            //$viewType = $request->get('viewType');
            $tplName = $request->get('tplName');
            $code = $request->get('code');
            $id = $request->get('id');

            $critereVente = $request->get('criteria0');
            $critereTypeDeBien = $request->get('criteria1');
            $critereSurface = $request->get('criteria61');
            $critereSecteur = $request->get('criteria3');

            //le numero de criteria ici correspond a l id du field 
            $criteres = array("criteria0" => $critereVente, "criteria1" => $critereTypeDeBien, "criteria61" => $critereSurface, "criteria2" => $critereSecteur);
            //dump($criteres);
            $reference = $request->get('reference');

            $isPromoted = $request->get('isPromoted');

            $limit = $request->get('limit');
            $order = $request->get('order');
            $direction = $request->get('direction');
            $fieldFilters = $request->get('extrafield');

            $noElementText = "Aucun élement";

            if (!is_null($request->get('noElementText'))) {
                $noElementText = $request->get('noElementText');
            }

            $viewPath = "components/content-collection/$collectionTpl.html.twig";

            if (!is_null($id)) {
                $viewPath = $app['config']['app']['theme'] . "/views/page-personnalisee/$collectionTpl.html.twig";
            }
            try {
                $annonceHelper = new Vertuoz\Utils\AnnonceHelper($apiClient);

                $params = array(
                    "id" => $id,
                    "limit" => $limit,
                    "order" => $order,
                    "code" => $code,
                    "isPromoted" => $isPromoted,
                    "direction" => $direction,
                    "language" => $language,
                    "extrafield" => $fieldFilters,
                    "reference" => $reference
                );
                //check criteres vides
                if (empty($reference)) {

                    foreach ($criteres as $key => $value) {

                        if (!is_null($value) && !empty($value) && $value != "null" && $value != "undefined") {
                            $params[$key] = $value;
                        }
                    }
                    $params["reference"] = null;
                }

                //dump($params);
                $contents = $annonceHelper->findAll($params);


                if (empty($contents)) {

                    $viewPath = "views/errors/annonce-notfound.html.twig";
                    return $app['twig']->render($viewPath, array("errorMessage" => "Annonces non trouvées"));
                }

                $twigData["isPromoted"] = $isPromoted;
                $twigData["reference"] = $reference;
                $twigData["language"] = $language;
                $twigData["contents"] = $contents;
                $twigData["viewType"] = $viewType;
                $twigData["tplName"] = $tplName;
                $twigData["areaName"] = $areaName;
                $twigData["no_element_text"] = $noElementText;
                $twigData['criteria0'] = $critereVente;
                $twigData['criteria1'] = $critereTypeDeBien;
                $twigData['criteria61'] = $critereSurface;
                $twigData['criteria3'] = $critereSecteur;
                //dump($twigData);die();
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                exit($e->getMessage());
            }

            return $app['twig']->render($viewPath, $twigData);
        })
        ->value('viewType', null)->bind('render-annonces');

/**
 * liste d'annonces
 */
$app->match('/{_locale}/liste-annonces', function ( Request $request) use ($app) {
            //dump($_POST);

            $twigData = array();
            $language = $request->get('_locale');
            $critereVente = $request->get('Type_de_transaction');
            $critereTypeDeBien = $request->get('Type_de_bien');
            $critereSurface = $request->get('Surface');
            $critereSecteur = $request->get('Secteur');
            $reference = $request->get('reference');
            $isPromoted = $request->get('isPromoted');
            $tplName = "vertical-blog-cards-item";
            $twigCollectionTpl = "vertical-blog-cards";
            //recuperation des criteres de recherche
            $apiClient = $app["api-client"];

            $twigData['reference'] = $reference;
            $twigData['isPromoted'] = $isPromoted;

            //criteres annonces a afficher
            $twigData['criteria0'] = $critereVente;
            $twigData['criteria1'] = $critereTypeDeBien;
            $twigData['criteria61'] = $critereSurface;
            $twigData['criteria3'] = $critereSecteur;
            $twigData['tplName'] = $tplName;
            $twigData['twigCollectionTpl'] = $twigCollectionTpl;
            //dump($twigData);
            $twigData['language'] = $language;
            $collectionTpl = "listeAnnonces";

            //dump($twigData);
            $tplPath = $app['config']['app']['theme'] . "/views/$collectionTpl.html.twig";

            return $app['twig']->render($tplPath, $twigData);
        })
        ->bind('liste-annonces');
/**
 * redirect url
 */
$app->get('/{_locale}/{slug}', function ( $slug, Request $request) use ($app) {


            define('BATIMENT', "Bâtiments d’activités");
            define('ENTREPOT', "Entrepôts Logistiques");
            define('BUREAU', "Bureaux");
            define('VENTE', "Vente");
            define('LOCATION', "Location");
            define('VAUCLUSE', "Vaucluse");
            define('BOUCHEDURHONE', "Bouches du Rhône");
            define('MR_BOUCHEDURHONE', "Bouches-du-Rhone");

            $messageForm = $request->get('messageForm');
            $language = $request->get('_locale');
            $twigData = array();
            $slugExploded = explode("-", str_replace(".html", "", $slug));
            //dump($slugExploded);
            $organiseId = array_pop($slugExploded);
            $organiseSingleId = $slugExploded[0];
            $collectionTpl = "listeAnnonces";
            $tplName = "vertical-blog-cards-item";
            $twigCollectionTpl = "vertical-blog-cards";

            //recuperation des parametres si annonce au singulier
            if ($organiseSingleId === "annonce") {

                $apiClient = $app["api-client"];
                $twigData = array();
                $collectionTpl = "fiche-produit-fp2i";

                try {

                    $annonceHelper = new Vertuoz\Utils\AnnonceHelper($apiClient);
                    $params = array("id" => $organiseId, "language" => $language);
                    $contents = $annonceHelper->findAll($params);

                    if (empty($contents)) {
                        $viewPath = "views/errors/annonce-notfound2.html.twig";
                        //dump("pas d'annonces");
                        return $app['twig']->render($viewPath, array("errorMessage" => "Annonces non trouvées"));
                    }

                    //icpe rubrique
                    $arrIcpe = array();
                    if (isset($contents[0]["fields"][5][1]["valeur"])) {
                        foreach ($contents[0]["fields"][5][1]["valeur"] as $key => $icpe) {

                            switch ($icpe) {
                                case 1510:
                                    $arrIcpe[$key]['content'] = 'Stockage de matières, produits ou substances combustibles dans les entrepôts couverts';
                                    break;
                                case 1511:
                                    $arrIcpe[$key]['content'] = 'Entrepôts frigorifiques';
                                    break;
                                case 1530:
                                    $arrIcpe[$key]['content'] = 'Dépot de papiers, cartons ou matériaux combustibles analogues';
                                    break;
                                case 1532:
                                    $arrIcpe[$key]['content'] = 'Bois sec ou matériaux combustibles analogues y compris les produits finis conditionnés (dépot de) à l\'exception des établissements recevant du public';
                                    break;
                                case 2662:
                                    $arrIcpe[$key]['content'] = 'Stockage de polymères';
                                    break;
                                case 2663:
                                    $arrIcpe[$key]['content'] = 'Stockage de pneumatiques et produits composés d\'au moins 50% de polymères';
                                    break;
                                case 4802:
                                    $arrIcpe[$key]['content'] = 'Fabrication, emploi, stockage de gaz à effet de serre fluorés';
                                    break;
                                 case 2925:
                                    $arrIcpe[$key]['content'] = 'Ateliers de charge d’accumulateurs.';
                                    break;
                            }

                            if (isset($arrIcpe[$key])) {
                                $arrIcpe[$key]['numero'] = $icpe;
                            }
                        }
                        $contents[0]["fields"][5][1]["valeur"] = $arrIcpe;
                    }
                    //permet d'envoyer le lien par mail
                    $lienMailUrl = $request->getScheme() . "://" . $request->getHttpHost() . "/" . $request->get("_locale") . "/" . $slug;

                    $twigData["lienMailUrl"] = $lienMailUrl;
                    $twigData["contents"] = $contents;
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    exit($e->getMessage());
                }
                $twigData['messageForm'] = $messageForm;
                $twigData['language'] = $language;
                $tplPath = $app['config']['app']['theme'] . "/views/page-personnalisee/$collectionTpl.html.twig";

                return $app['twig']->render($tplPath, $twigData);
            }

            switch ($organiseId) {

                case 1940 :
                    $twigData['criteria1'] = BATIMENT;
                    break;

                case 1941 :
                    $twigData['criteria1'] = BATIMENT;
                    $twigData['criteria0'] = VENTE;
                    break;

                case 1942 :
                    $twigData['criteria1'] = BATIMENT;
                    $twigData['criteria0'] = LOCATION;
                    break;

                case 1988 :
                    $twigData['criteria1'] = BATIMENT;
                    $twigData['criteria0'] = VENTE;
                    $twigData['criteria3'] = VAUCLUSE;
                    break;

                case 1989 :
                    $twigData['criteria1'] = BATIMENT;
                    $twigData['criteria0'] = VENTE;
                    $twigData['criteria3'] = BOUCHEDURHONE;
                    $twigData['mr_boucheDuRhone'] = MR_BOUCHEDURHONE;
                    break;

                case 1986 :
                    $twigData['criteria1'] = BATIMENT;
                    $twigData['criteria0'] = LOCATION;
                    $twigData['criteria3'] = BOUCHEDURHONE;
                    $twigData['mr_boucheDuRhone'] = MR_BOUCHEDURHONE;
                    break;

                case 1985 :
                    $twigData['criteria1'] = BATIMENT;
                    $twigData['criteria0'] = LOCATION;
                    $twigData['criteria3'] = VAUCLUSE;
                    break;

                case 1943 :
                    $twigData['criteria1'] = ENTREPOT;
                    break;

                case 1945 :
                    $twigData['criteria1'] = ENTREPOT;
                    $twigData['criteria0'] = LOCATION;
                    break;

                case 1947 :
                    $twigData['criteria1'] = ENTREPOT;
                    $twigData['criteria0'] = VENTE;
                    break;

                case 1990 :
                    $twigData['criteria1'] = ENTREPOT;
                    $twigData['criteria0'] = VENTE;
                    $twigData['criteria3'] = VAUCLUSE;
                    break;

                case 1992 :
                    $twigData['criteria1'] = ENTREPOT;
                    $twigData['criteria0'] = LOCATION;
                    $twigData['criteria3'] = VAUCLUSE;
                    break;

                case 1991 :
                    $twigData['criteria1'] = ENTREPOT;
                    $twigData['criteria0'] = LOCATION;
                    $twigData['criteria3'] = BOUCHEDURHONE;
                    $twigData['mr_boucheDuRhone'] = MR_BOUCHEDURHONE;
                    break;

                case 1993 :
                    $twigData['criteria1'] = ENTREPOT;
                    $twigData['criteria0'] = VENTE;
                    $twigData['criteria3'] = BOUCHEDURHONE;
                    $twigData['mr_boucheDuRhone'] = MR_BOUCHEDURHONE;
                    break;

                case 1946 :
                    $twigData['criteria1'] = BUREAU;
                    break;

                case 1944 :
                    $twigData['criteria1'] = BUREAU;
                    $twigData['criteria0'] = VENTE;
                    break;

                case 1994 :
                    $twigData['criteria1'] = BUREAU;
                    $twigData['criteria0'] = VENTE;
                    $twigData['criteria3'] = VAUCLUSE;
                    break;

                case 1995 :
                    $twigData['criteria1'] = BUREAU;
                    $twigData['criteria0'] = VENTE;
                    $twigData['criteria3'] = BOUCHEDURHONE;
                    $twigData['mr_boucheDuRhone'] = MR_BOUCHEDURHONE;
                    break;

                case 1948 :
                    $twigData['criteria1'] = BUREAU;
                    $twigData['criteria0'] = LOCATION;
                    break;

                case 1996 :
                    $twigData['criteria1'] = BUREAU;
                    $twigData['criteria0'] = LOCATION;
                    $twigData['criteria3'] = VAUCLUSE;
                    break;

                case 1997 :
                    $twigData['criteria1'] = BUREAU;
                    $twigData['criteria0'] = LOCATION;
                    $twigData['criteria3'] = BOUCHEDURHONE;
                    $twigData['mr_boucheDuRhone'] = MR_BOUCHEDURHONE;
                    break;

                //info-flash              
                case 1954 :
                    //$twigData['isPromoted'] = 1;   
                    return $app->redirect('/' . $language . '/flashinfo');
                    break;
                

                default :
                    $twigData['criteria0'] = null;
                    $twigData['criteria1'] = null;
                    $twigData['criteria61'] = null;
                    $twigData['criteria3'] = null;
                    $twigData['reference'] = null;
                    $twigData['isPromoted'] = 0;
                    break;
            }


            $twigData['language'] = $language;
            $twigData['twigCollectionTpl'] = $twigCollectionTpl;
            $twigData['tplName'] = $tplName;
            //dump($tplName);
            //dump($twigData);die();
            $tplPath = $app['config']['app']['theme'] . "/views/$collectionTpl.html.twig";

            return $app['twig']->render($tplPath, $twigData);
        })
        ->assert('slug', '^annonce.+$')
        ->bind('urlAnnonces');

$app->get('/render-recherche', function (Request $request) use ($app) {
            /**
             * @param target : cible (methode ajax ou route liste annonce)
             * @param buttonType : en fonction de la cible submit ou button
             * @param place : CSS en fonction de la page ou le form se trouve
             */
            $language = $request->get('language');
            //dump($language);
            $apiClient = $app["api-client"];
            $immocriteriaHelper = new \Vertuoz\Utils\ImmocriteriaHelper($apiClient);

            $searchInfo = $immocriteriaHelper->findAll(array("language" => $language));

            //recuperation des valeurs pour recherche en anglais
            $searchValue = $immocriteriaHelper->findAll(array("language" => 'fr'));

            $target = $request->get('target');
            $buttonType = $request->get('buttonType');
            $critereVente = $request->get('criteria0');
            $critereTypeDeBien = $request->get('criteria1');
            $critereSurface = $request->get('criteria61');
            $critereSecteur = $request->get('criteria3');
            $mr_bdr = $request->get('mr_boucheDuRhone');

            $twigData['criteria'] = array($searchInfo[0], $searchInfo[1], $searchInfo[2], $searchInfo[3]);
            $twigData['searchValue'] = array($searchValue[0], $searchValue[1], $searchValue[2], $searchValue[3]);

            $twigData['search'] = $searchInfo;

            if (!is_null($request->get('place'))) {
                $place = $request->get('place');
            } else {
                $place = "";
            }

            $twigData['language'] = $language;
            $twigData['place'] = $place;
            $twigData['target'] = $target;
            $twigData['buttonType'] = $buttonType;

            //parametres de la recherche pour selected
            $twigData['criteria0'] = $critereVente;
            $twigData['criteria1'] = $critereTypeDeBien;
            $twigData['criteria61'] = $critereSurface;
            $twigData['criteria3'] = $critereSecteur;
            $twigData['mr_boucheDuRhone'] = $mr_bdr;

            $tplPath = $app['config']['app']['theme'] . "/views/recherche.html.twig";
            //dump($twigData);
            return $app['twig']->render($tplPath, $twigData);
        })
        ->bind('render-recherche');

$app->get('/render-map', function (Request $request) use ($app) {
            $lat = $request->get('lat');
            $lng = $request->get('lng');
            $title = $request->get('title');
            $tplMapName = $request->get('tplMapName');

            $twigData['lat'] = $lat;
            $twigData['lng'] = $lng;
            $twigData['title'] = $title;
            //'contact' pour marker, sinon zone
            $twigData['tplMapName'] = $tplMapName;

            $tplPath = $app['config']['app']['theme'] . "/views/map.html.twig";
            //dump($tplPath);
            return $app['twig']->render($tplPath, $twigData);
        })
        ->bind('render-map');

$app->get('/{_locale}/flashinfo', function (Request $request) use ($app) {

            $apiClient = $app["api-client"];
            $language = $request->get('_locale');

            $annonceHelper = new Vertuoz\Utils\AnnonceHelper($apiClient);
            $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
            $content = $contentHelper->getAllByArea("flash-infos");

            foreach ($content as $key => $contenu) {
                $twigData['flashContent'][$key] = $contenu;

                foreach ($contenu['extrafields'] as $reference) {
                    if (!empty($reference) && $reference != " ") {
                        $params = array("reference" => $reference,
                            "language" => $language);
                        $annonce = $annonceHelper->findAll($params);
                        //$annonce = $annonceHelper->getById($annonce[0]['id']);
                        $twigData['flashContent'][$key]['annonceId'] = $annonce[0]['id'];
                        $twigData['flashContent'][$key]['photo1file'] = $annonce[0]['photo1File'];
                        $twigData['flashContent'][$key]['url'] = rewriteStringToUrl($annonce[0]['fields'][0][0]['valeur'] . "-" . $annonce[0]['fields'][0][1]['valeur'] . "-" . $annonce[0]['fields'][0][2]['valeur'] . "-" . $annonce[0]['id']);
                    }
                }
            }
            $tplName = "horizontal-blog-cards-fp2i";
            $twigCollectionTpl = "horizontal-blog-cards";

            $twigData['language'] = $language;
            $twigData['twigCollectionTpl'] = $twigCollectionTpl;
            $twigData['tplName'] = $tplName;

            $tplPath = $app['config']['app']['theme'] . "/views/flashinfos.html.twig";
            //dump($twigData['flashContent']);die(); 
            return $app['twig']->render($tplPath, $twigData);
            //dump($content);die();
        })
        ->bind('flashinfo');


$app->get('/render-flashinfo', function (Request $request) use ($app) {

            $apiClient = $app["api-client"];
            $language = $request->get('language');

            $annonceHelper = new Vertuoz\Utils\AnnonceHelper($apiClient);
            $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
            $params = array(
                "limit" => 3,
                "areaName" => "flash-infos",
                "order" => "date",
                "direction" => "desc"
            );
            $content = $contentHelper->listContents($params);

            foreach ($content as $key => $contenu) {
                $twigData['flashContent'][$key] = $contenu;

                foreach ($contenu['extrafields'] as $reference) {
                    if (!empty($reference) && $reference != " ") {
                        $params = array("reference" => $reference);
                        $annonce = $annonceHelper->findAll($params);
                        $twigData['flashContent'][$key]['annonceId'] = $annonce[0]['id'];
                        $twigData['flashContent'][$key]['photo1file'] = $annonce[0]['photo1File'];
                    }
                }
            }

            $twigData['language'] = $language;


            $tplPath = $app['config']['app']['theme'] . "/components/content-collection/vertical-blog-cards-flash.html.twig";
            //dump($twigData['flashContent']);die; 
            return $app['twig']->render($tplPath, $twigData);
            //dump($content);die();
        })
        ->bind('render-flashinfo');

/**
 * Contact
 */
$app->match('/{_locale}/contactFiche', function (Request $request) use ($app) {
            //dump($request);

            $language = $request->get('_locale');
            //die($language);
            $apiClient = $app["api-client"];
            $twigData = array();
            $id = $request->get('id');
            $criteria0 = $request->get('criteria0');
            $criteria1 = $request->get('criteria1');
            $criteria3 = $request->get('criteria3');
            $twigData['criteria0'] = $criteria0;
            $twigData['criteria1'] = $criteria1;
            $twigData['criteria3'] = $criteria3;
            $twigData['id'] = $id;
            $twigData['language'] = $language;
            //dump($id);
            $app['active_page'] = '/contactFiche';
//
//            try {
//                $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
//                $content = $contentHelper->getOneByCode("contact",$language);
//                $twigData['content'] = $content;
//                $twigData['content']['title'] = "Contact";
//                $twigData["analytics_account"] = $app['config']['app']['analytics_account'];
//            } catch (\GuzzleHttp\Exception\ClientException $e) {
//                exit($e->getMessage());
//            }

            $labelMessage = 'votre message *';
            $labelNom = 'votre nom *';
            $labelMail = 'votre adresse e-mail *';
            $labelBtn = 'Envoyer';
            $labelRgpd = "J'accepte l'enregistrement de mes données personnelles pour le traitement de ma demande.";

            if ($language != 'fr') {
                $labelMessage = 'your message *';
                $labelNom = 'your name *';
                $labelMail = 'your e-mail *';
                $labelBtn = 'Send';
                $labelRgpd = 'you accept the conditions';
            }

            $isSpam = false;
            $isSpamTime = 5;
            $data = array();

            $form = $app['form.factory']->createBuilder(FormType::class, $data)
                    ->add('name', TextType::class, array(
                        'label' => $labelNom,
                        'required' => true,
                        'constraints' => array(
                            new Assert\NotBlank(array(
                                'message' => 'Veuillez indiquer votre nom',
                                    ))
                        ),
                        'attr' => array('class' => 'inputContactAnnonce')
                    ))
                    ->add('email', EmailType::class, array(
                        'label' => $labelMail,
                        'required' => true,
                        'constraints' => array(
                            new Assert\Email(array(
                                'message' => 'Veuillez indiquer une adresse e-mail valide',
                                'checkMX' => false,
                                    ), new Assert\NotBlank(array(
                                'message' => 'Veuillez indiquer votre adresse e-mail',
                                    ))
                            )),
                        'attr' => array('class' => 'inputContactAnnonce')
                    ))
                    ->add('message', TextareaType::class, array(
                        'label' => $labelMessage,
                        'required' => true,
                        'constraints' => array(
                            new Assert\NotBlank(array(
                                'message' => 'Veuillez indiquer un message',
                                    )),
                        ),
                    ))
                    ->add('rgpd', CheckboxType::class, array(
                        'label' => $labelRgpd,
                        'required' => true,
                    ))
                    // Anti spam
                    ->add('fax', HiddenType::class, array(
                        'label' => "Votre fax",
                        'required' => false,
                        'attr' => array(
                            'tabindex' => '-1',
                            'autocomplete' => 'off'
                        )
                    ))
                    // Anti spam
                    ->add('portable', HiddenType::class, array(
                        'label' => "Votre portable",
                        'data' => base64_encode(time()),
                        'required' => true,
                        'attr' => array(
                            'tabindex' => '-1',
                            'autocomplete' => 'off'
                        )
                    ))
                    ->add('submit', SubmitType::class, [
                        'label' => $labelBtn,
                        'attr' => array('class' => 'btn btn--sm btn--primary btnContact')
                    ])
                    ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $data = $form->getData();
                //die(var_dump($data));

                $title = $app['config']['app']['contact']['email_title'];
                $from = $app['config']['app']['contact']['from'];
                $to = $app['config']['app']['contact']['to'];

                //$data = $form->getData();

                $fax = $data['fax'];
                $portable = base64_decode($data['portable']);
                $name = $data['name'];
                $message = $data['message'];

                $recaptcha = new ReCaptcha('6LcGD0kUAAAAACQaCIugyGDmZyyCtrEaIQf7q5zB');
                $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());

                if (!$resp->isSuccess()) {
                    // Do something if the submit wasn't valid ! Use the message to show something
                    foreach ($resp->getErrorCodes() as $code) {
                        //die ('capt');
                        $message .= $code;
                    }
                } else {

                    // Anti spam
                    if (strlen($fax) > 0) {
                        // Le champ fax a été rempli : SPAM
                        $isSpam = true;
                    } elseif ($portable + $isSpamTime > time()) {
                        // Le formulaire a été soumis en moins de 5 secondes : SPAM
                        $isSpam = true;
                    } else {

                        $body = "";
                        $body .= "Nom : " . $name . "<br />";
                        $body .= "Email : " . $data['email'] . "<br />";
                        $body .= "<br />";
                        $body .= nl2br($message);

                        $mail = \Swift_Message::newInstance()
                                ->setSubject($title)
                                ->setFrom(array($from))
                                ->setTo(array($to))
                                ->setBody($body, 'text/html');

                        $app['mailer']->send($mail);

                        $twigData['sended'] = true;
                        $twigData["language"] = $language;

                        if ($id > 0) {
                            return $app->redirect('/' . $language . '/annonce-' . $id . '.html?' . http_build_query(array('messageForm' => 1)));
                        }

                        return $app['twig']->render('views/contact.html.twig', $twigData);
                    }
                }
            }

            $twigData["language"] = $language;
            $twigData['spam'] = $isSpam;
            $twigData['form'] = $form->createView();

            if ($request->get('collectionTpl')) {
                $collectionTpl = $request->get('collectionTpl');
            } else {
                $collectionTpl = "contact";
            }
            $tplPath = $app['config']['app']['theme'] . '/views/' . $collectionTpl;
            $url = "/contact";

            $tplOverride = Vertuoz\Utils\TemplateHelper::getTemplateOverrideByUrl($url, $app['config']['pages']);
            if (!is_null($tplOverride)) {
                $tplPath = $tplOverride;
            }
            if ($form->isSubmitted() && $id > 0) {
                return $app->redirect('/' . $language . '/annonce-' . $app['slugify']->slugify($criteria0) . '-' . $app['slugify']->slugify($criteria1) . '-' . $app['slugify']->slugify($criteria3) . '-' . $id . '.html?' . http_build_query(array('messageForm' => 2)));
            }

            return $app['twig']->render($tplPath . '.html.twig', $twigData);
        })
        ->bind('contactFiche');

$app->get('/render-pdf/{id}', function (Request $request, $id) use ($app) {

            $apiClient = $app["api-client"];
            $language = $request->get('language');
            
            $nopdf = $request->get('nopdf');

            //recuperation de l'annonce
            $annonceHelper = new Vertuoz\Utils\AnnonceHelper($apiClient);
            $annonces = $annonceHelper->findAll(array('id' => $id,
                'language' => 'fr'));
            $annonce = $annonces[0];

            //contenu et dessertes
            //$commentaireDesserte = explode('<!-- pagebreak -->', $annonce['content']);
            $commentaire = $annonce['fields'][8][0]['valeur']; //$commentaireDesserte[0];
            $desserte    = $annonce['fields'][8][1]['valeur'];
            
            //si le contenu desserte existe le pagebreack se trouvant entre span et p il faut les ouvrirs et fermer pour chaque variable
//            if (!empty($commentaireDesserte[0]) && !empty($commentaireDesserte[1])) {
//                $commentaire = $commentaireDesserte[0] . '</span></p>';
//                $desserte = '<p><span>' . $commentaireDesserte[1];
//            }
            

            //$annonce['fields'][0][1]['valeur'];
            //dump($annonce);die();
            //construction du html

            
            $style = "<style>
                    table{
                    height:209mm;
                    }
                    tr{
                    bordercolor:black;
                    }
                    td{
                    width:93mm;
                    margin:0mm;
                    padding:0mm;
                    }
                    tr{
                    margin:0mm;
                    padding:0mm;
                    }
                    .border{
                    border: 0.5mm solid #655a4f;
                    }
                    p{
                    margin:1mm;
                    }
                    .desserte p {
                     margin:0mm;
                    }
                    .image{
                    width:96mm;
                    height:63mm;
                    }
                    .marron{
                    background-color:#655a4f;
                    color:white;
                    text-align:center;
                    }
                    .beige{
                    background-color:#efeed7;
                    color:#655a4f;
                    text-align:center;
                    font-weight:normal!important;
                    }
                    .gris{
                    background-color:#b3a79c;
                    color:white;
                    text-align:center;
                    }
                    .table1{
                    position:absolute;
                    left:0mm;
                    border:solid 2px #655a4f;
                    }
                    .table2{
                    position:absolute;
                    left:97mm;
                    top:0mm;
                    border:solid 2px #655a4f;
                    border-left:none;
                    border-right:none;
                    }
                    .table3{
                    position:absolute;
                    left:190mm;
                    border:solid 2px #655a4f;
                    }
                    .titre{
                     height:7mm;
                    vertical-align:center;
                    }
                    .conseil{
                    position:relative;
                    float:right;
                    }
                    </style>";

            
            //Récupération image map statique
            $map = "<img class='image' src='http://www.fp2i-entreprises.com/mapImg/mapBureau.png'>";
            if((strlen(trim($annonce['fields'][0][4]['valeur'])) + strlen(trim($annonce['fields'][0][5]['valeur']))) > 0) {
                //$url = "https://maps.googleapis.com/maps/api/staticmap?center=" . $annonce['fields'][0][4]['valeur'] . "," . $annonce['fields'][0][5]['valeur'] . "&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C" . $annonce['fields'][0][4]['valeur'] . "," . $annonce['fields'][0][5]['valeur'] . "&key=AIzaSyBWDCZvF_wF0Do3swQOfbOGj3Go6PHY2TQ";
                $url = "https://maps.googleapis.com/maps/api/staticmap?center=" . $annonce['fields'][0][4]['valeur'] . "," . $annonce['fields'][0][5]['valeur'] . "&zoom=13&size=420x260&maptype=roadmap&key=AIzaSyBWDCZvF_wF0Do3swQOfbOGj3Go6PHY2TQ";

                $desFolder = $_SERVER['DOCUMENT_ROOT'] . '/mapImg/';

                $imageName = $annonce['reference'].'.png';
                $imagePath = $desFolder.$imageName;
                file_put_contents($imagePath,file_get_contents($url));

                $map = "<img class='image' src='http://www.fp2i-entreprises.com/mapImg/".$imageName."'>";
            }

            // vente ou loc
            $venteLoc = "A LOUER";
            $vente = false;
            $labelVenteLoc   = 'Loyer Mensuel Global HT/HD';
            $labelVenteLocM2 = 'Loyer Annuel Global HT/HD/m2';

            if ($annonce['fields'][0][0]['valeur'] == 'Vente') {
                $venteLoc = "A VENDRE";
                $vente = true;
                $labelVenteLoc = 'Prix de Vente HT/HD';
                $labelVenteLocM2 = 'Prix de vente HT/HD/m2';
            }
            
            
            //les prestations sont differentes si bureaux            
            $prestations = $annonce['content'];


            /*if ($annonce['fields'][0][1]['valeur'] != 'Bureaux') {
                $annonce['fields'][2][1]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][1]['label'] . "</strong> : " . $annonce['fields'][2][1]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][2][4]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][4]['label'] . "</strong> : " . $annonce['fields'][2][4]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][2][5]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][5]['label'] . "</strong> : " . $annonce['fields'][2][5]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][2][3]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][3]['label'] . "</strong> : " . $annonce['fields'][2][3]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][2][6]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][6]['label'] . "</strong> : " . $annonce['fields'][2][6]['valeur'] . "</p>" : $prestations .= '';
                
                $prestations .= "<p>".$annonce['fields'][8][1]['valeur']."</p>";
                        
                $annonce['fields'][4][1]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][1]['label'] . "</strong> : " . $annonce['fields'][4][1]['valeur'] . " m²</p>" : $prestations .= '';
                $annonce['fields'][4][2]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][2]['label'] . "</strong> : " . $annonce['fields'][4][2]['valeur'] . " m²</p>" : $prestations .= '';
                $annonce['fields'][4][3]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][3]['label'] . "</strong> : " . $annonce['fields'][4][3]['valeur'] . " m²</p>" : $prestations .= '';
                $annonce['fields'][4][9]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][9]['label'] . "</strong> : " . $annonce['fields'][4][9]['valeur'] . " m²</p>" : $prestations .= '';
                $annonce['fields'][4][13]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][13]['label'] . "</strong> : " . $annonce['fields'][4][13]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][3][10]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][3][10]['label'] . "</strong> : " . $annonce['fields'][3][10]['valeur'] . "</p>" : $prestations .= '';
            } else {
                
                $annonce['fields'][2][3]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][3]['label'] . "</strong> : " . $annonce['fields'][2][3]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][2][8]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][2][8]['label'] . "</strong> : " . $annonce['fields'][2][8]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][3][11]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][3][11]['label'] . "</strong> : " . $annonce['fields'][3][11]['valeur'] . "</p>" : $prestations .= '';
                
                $prestations .= "<p>".$annonce['fields'][8][1]['valeur']."</p>";
                
                $annonce['fields'][4][0]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][0]['label'] . "</strong> : " . $annonce['fields'][4][0]['valeur'] . "</p>" : $prestations .= '';
                $annonce['fields'][4][8]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][8]['label'] . "</strong> : " . $annonce['fields'][4][8]['valeur'] . " m²</p>" : $prestations .= '';
                $annonce['fields'][4][10]['valeur'] != '' ? $prestations .= "<p><strong>- " . $annonce['fields'][4][10]['label'] . "</strong> : " . $annonce['fields'][4][10]['valeur'] . " m²</p>" : $prestations .= '';
            }*/


            //dump($url . http_build_query($bits) );die;

            $content = '<page backbottom="0mm" backtop="3mm" backleft="0mm" backright="0mm" pageset="new">';


            $content .= "<table class='table1' cellspacing='0'>
                            <tr>
                                <td class='beige' style='width:93mm;height:15mm'><h1>" . $annonce['fields'][0][1]['valeur'] . "</h1></td>
                            </tr>
                            <tr>
                                <td><img class='image' src='" . $annonce['photo1File'] . "'></td>
                            </tr> 
                            
                            <tr>
                                <td class='marron titre' style=''>
                                    <p>Commentaires</p>                                    
                                </td> 
                            </tr>
                            <tr>
                                <td class='beige' style='height:66mm;margin-bottom:0mm!important;vertical-align:top;'><div style='padding:10px!important;width:90mm;'>" . strip_tags(preg_replace('/(<[^>]+) style=".*?"/i', '$1', str_replace('&nbsp;','',str_replace('m&sup2;','',$commentaire))), '<strong><br></br>') . "</div></td>
                            </tr> 
                            <tr>
                                <td class='marron titre' style=''>
                                    <p>Disponibilité</p>                                    
                                </td> 
                            </tr>
                            <tr>
                                <td class='beige' style='height:7mm;'>" . $annonce['fields'][1][2]['valeur'] . "</td>
                            </tr>  
                            <tr>
                                <td style='background-color:#efeed7;height:25mm;border-top:solid 1px #655a4f;'>
                                    <div style='background-color:#efeed7;'>
                                    <img style='float:left;width:32mm;height:auto;' src='https://rootelo.elocms.com/documents/users/271/editor/pics/fp2i/logo-fp2i.png'>
                                    <div style='text-align:center;width:62mm;color:#655a4f;top:2mm;position:relative;'>CONSEIL EN IMMOBILIER D'ENTREPRISE</div>
                                    <div style='text-align:center;width:62mm;color:#655a4f;top:8mm;position:relative;
                                    '>www.fp2i-entreprises.com</div>
                                    </div>
                                </td>
                            </tr>  
                        </table>
                        <table class='table2' cellspacing='0'>
                            <tr>
                                <td class='marron' style='width:93mm;height:15mm'><h1>" . $annonce['fields'][0][2]['valeur'] . "</h1></td>
                            </tr>
                            <tr>
                                <td class='marron titre' style=''>
                                    <p>Prestations</p>                                    
                                </td> 
                            </tr>
                            <tr>
                                <td style='height:143mm;max-height:143mm!important;vertical-align:top;'><div style='padding:5px;text-align:justify;'>" . preg_replace('/<span[^>]+\>|<\/span>/i', '',str_replace('<p>&nbsp;</p>','<br>',$prestations)) . "</div></td>
                            </tr>  
                            <tr>
                                <td class='marron' style='height:25.5mm'>
                                    <p>121 Rue Jean Dausset</p>
                                    <p>Technopôle d'Activité AGROPARC - MFT</p>
                                    <p>BP 51251</p>
                                    <p>84911 AVIGNON CEDEX 09</p>
                                </td>
                            </tr> 
                        </table>
                        <table class='table3' cellspacing='0'>
                            <tr>
                                <td class='gris' style='width:93mm;height:15mm'><h1>" . $venteLoc . "</h1></td>
                            </tr>
                            <tr>
                               <td class='marron titre' style=''>
                                    <p>Conditions juridiques et financières</p>                                    
                                </td> 
                            </tr>
                            <tr>
                                <td style='height:40mm;vertical-align:top;'>";
            
                                if(strlen(trim($annonce['fields'][6][0]['valeur'])) > 0) {
                                    $buf = is_numeric($annonce['fields'][6][0]['valeur']) ? number_format($annonce['fields'][6][0]['valeur'], 0, '.', ' ')." €" : $annonce['fields'][6][0]['valeur'];
                                    $content .= "<p><strong>- " . $labelVenteLoc . " : " . $buf . "</strong></p>";
                                }
                                
                                if(strlen(trim($annonce['fields'][6][4]['valeur'])) > 0) {
                                    $buf = is_numeric($annonce['fields'][6][4]['valeur']) ? number_format($annonce['fields'][6][4]['valeur'], 0, '.', ' ')." € m2" : $annonce['fields'][6][4]['valeur'];
                                    $content .= "<p><strong>- " . $labelVenteLocM2 . " : " . $buf . "</strong></p>";
                                }

                                if(strlen(trim($annonce['fields'][6][1]['valeur'])) > 0) {
                                    $buf = is_numeric($annonce['fields'][6][1]['valeur']) ? number_format($annonce['fields'][6][1]['valeur'], 0, '.', ' ')." €" : $annonce['fields'][6][1]['valeur'];
                                    $content .= "<p><strong>- " . $annonce['fields'][6][1]['label'] . "</strong> : " . $buf . "</p>";
                                }
                                
//<strong>- " . $annonce['fields'][6][3]['label'] . "</strong> : 
                                
                                //Si on a des honoraires
                                if(strlen(trim($annonce['fields'][6][2]['valeurPourcent'])) > 0 || strlen(trim($annonce['fields'][6][3]['valeurPourcent'])) > 0 ) {
                                    
                                    $content .= "<p><strong>- " . $annonce['fields'][6][2]['label'] . "</strong> :<br><br>";

                                    if ($annonce['fields'][0][0]['valeur'] == 'Vente') {

                                        if(strlen(trim($annonce['fields'][6][2]['valeurPourcent'])) > 0)
                                            $content .= $annonce['fields'][6][2]['valeurPourcent'] . "% du montant de la Cession H.T/H.D à la charge du Vendeur,<br>"; //$annonce['fields'][6][3]['valeurLabel']
                                        
                                        if(strlen(trim($annonce['fields'][6][3]['valeurPourcent'])) > 0)
                                            $content .= $annonce['fields'][6][3]['valeurPourcent'] . "% du montant de la Cession H.T/H.D à la charge de l'Acquereur.";

                                    }
                                    
                                    if ($annonce['fields'][0][0]['valeur'] == 'Location') {

                                        if(strlen(trim($annonce['fields'][6][2]['valeurPourcent'])) > 0)
                                            $content .= $annonce['fields'][6][2]['valeurPourcent'] . "% du montant d’un Loyer Annuel HT à la charge du Bailleur,<br>"; //$annonce['fields'][6][3]['valeurLabel']
                                        
                                        if(strlen(trim($annonce['fields'][6][3]['valeurPourcent'])) > 0)
                                            $content .= $annonce['fields'][6][3]['valeurPourcent'] . "% du montant d’un Loyer Annuel H.T à la charge du Preneur.";

                                    }

                                    $content .= "</p>";
                                }
                                
                                $content .= "</td>
                            </tr>   
                            <tr>
                               <td class='marron titre' ><p>Dessertes</p></td> 
                            </tr>
                            <tr>
                                <td class='desserte' style='height:26mm;max-height:26mm;'><div style='padding:5px;'>" . strip_tags($desserte, '<strong><br></br>') . "</div></td>
                            </tr> 
                            <tr>
                               <td class='marron titre' style=''>
                                    <p>Géolocalisation</p>                                    
                                </td> 
                            </tr>
                            <tr>
                                <td><div class='image'>".$map."</div></td>
                            </tr> 
                            <tr>
                               <td class='marron titre' style=''>
                                    <p>Votre Interlocuteur :</p>                                    
                                </td> 
                            </tr>
                            <tr>
                                <td class='gris' style='height:18.5mm'>
                                <p>Franck TESTANIERE</p>
                                <p>Port : 0689852137</p>
                                <p>E-mail : f.testaniere@fp2i-entreprises.com</p>
                                </td>
                            </tr> 
                        </table>"
            ;
               

            $content .= '</page>';

            //return "<td><img class='image' src='".$map."' /></td>";die();

            if(!isset($nopdf)) {
                //var_dump($annonce['fields'][6][0]);die();
                $html2pdf = new HTML2PDF('L', 'A4', 'fr');//var_dump($html2pdf->WriteHTML($style));die();
                $html2pdf->WriteHTML($style . $content); //dump($style . $content);die;
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->Output($annonce['reference'].'.pdf', 'D');
            }
            else {
                echo $content;
            }
        })
		->bind('render-pdf');

$app->match('/{_locale}/liste-temoignage', function ( Request $request) use ($app) {
	$twigData = array();
		                $language = $request->get('_locale');
		                $twigData["language"] = $language;
				            $apiClient = $app["api-client"];
				            $contentHelper = new \Vertuoz\Utils\ContentHelper($apiClient);
					                $params = array(
								                "limit" => 100,
										                "areaName" => "carrousel",
												                "order" => "position",
														                "direction" => "desc"
																            );
					                $content = $contentHelper->listContents($params);
					                $twigData["contents"] = $content;    
							            $tplPath = $app['config']['app']['theme'] . "/views/page-personnalisee/page-temoignages.html.twig";
							            return $app['twig']->render($tplPath, $twigData);
								            })
										            ->bind('liste-temoignage');
