<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\CsrfServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use GuzzleHttp\Client;

$app = new Application();

$app->register(
        new GeckoPackages\Silex\Services\Config\ConfigServiceProvider(), array(
    'config.dir' => __DIR__ . '/../config',
    'config.format' => '%key%.yml',
        //'config.cache' => 'memcached',
        )
);
$app['cache.path'] = __DIR__ . '/../var/cache';

$app->register(new ServiceControllerServiceProvider());
$app->register(new TwigServiceProvider());

//slugify
//$app->register(new Cocur\Slugify\Bridge\Silex2\SlugifyServiceProvider());

$app->register(new FormServiceProvider());
$app->register(new CsrfServiceProvider());
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'), 'locale' => 'fr'
));

$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['swiftmailer.options'] = array(
'host' => 'smtp.gmail.com',
 'port' => 465,
 'username' => 'mettlingpot@gmail.com',
 'password' => '8J9mjbtx0',
 'encryption' => 'ssl',
 'auth_mode' => 'login',
);


$app->register(new HttpFragmentServiceProvider());

$app->register(new AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
    'assets.named_packages' => array(
        'css' => array('base_path' => '/css'),
        'images' => array('base_urls' => array($app['config']['app']['url'])),
    ),
));

$app->extend('twig.loader.filesystem', function($twigLoader, $app) {
    $twigLoader->addPath(realpath(__DIR__ . '/../templates/' . $app['config']['app']['theme'] . ""), '__main__');

    return $twigLoader;
});

$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...
    $twig->addExtension(new Twig_Extensions_Extension_Text($app));
    $twig->addExtension(new Twig_Extensions_Extension_Intl());
    return $twig;
});


$app['default_title'] = $app['config']['app']['default_title'];
$app['default_desc'] = $app['config']['app']['default_desc'];

if (isset($app['config']['app']['maps_key'])) {
    $app['maps_key'] = $app['config']['app']['maps_key'];
}

$app['locale'] = $app['config']['app']['locale'];
$app['theme'] = $app['config']['app']['theme'];
$app['style'] = $app['config']['app']['style'];
$app['analytics_account'] = $app['config']['app']['analytics_account'];

$app['header'] = null;
$app['footer'] = null;
$app['fonts'] = null;
$app['social'] = null;
$app['active_page'] = null;

if (isset($app['config']['app']['header'])) {
    $app['header'] = $app['config']['app']['header'];
}
if (isset($app['config']['app']['footer'])) {
    $app['footer'] = $app['config']['app']['footer'];
}
if (isset($app['config']['app']['fonts'])) {
    $app['fonts'] = $app['config']['app']['fonts'];
}
if (isset($app['config']['app']['social'])) {
    $app['social'] = $app['config']['app']['social'];
}

$apiBaseUrl = $app['config']['app']['api']['base_url'];
$apiToken = $app['config']['app']['api']['token'];

$headers = ['token' => $apiToken];
$client = new Client([
    'base_uri' => $apiBaseUrl,
    'headers' => $headers
        ]);

$app["api-client"] = new \Vertuoz\Utils\ApiHelper($client, "v" . $app['config']['app']['api']['version']);
return $app;

