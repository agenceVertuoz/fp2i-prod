<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vertuoz\Utils;

/**
 * Description of TemplateOverride
 *
 * @author vertuoz
 */
class TemplateHelper {

    //put your code here


    public static function getTemplateOverrideByUrl($url, $configuration) {
        $tplPath = null;
        if (isset($configuration["override"]["template"][$url])) {
            $tplPath = $configuration["override"]["template"][$url]["path"];
        }

        return $tplPath;
    }

}
