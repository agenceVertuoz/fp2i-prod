<?php

namespace Vertuoz\Utils;

class ApiHelper {

    protected $apiVersion;
    protected $guzzleClient;

    /**
     * 
     * @param \GuzzleHttp\Client $guzzleClient
     * @param type $apiVersion
     */
    public function __construct(\GuzzleHttp\Client $guzzleClient, $apiVersion) {
        $this->guzzleClient = $guzzleClient;
        $this->apiVersion   = $apiVersion;
    }

    /**
     * 
     * @param string $url
     * @return type
     */
    
    public function get($url) {
        $url        = $this->apiVersion . $url;
        $response   = $this->guzzleClient->get($url)->getBody()->getContents();
        return json_decode($response, true);
    }

}
