<?php

namespace Vertuoz\Utils;

class AnnonceHelper {

    protected $apiHelper;

    /**
     * 
     * @param \Vertuoz\Utils\ApiHelper $apiHelper
     */
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }

    /**
     * 
     * @param int $id
     * @return json
     */
    public function getById($id) {
        return $this->apiHelper->get('/annonces/' . $id);
    }
    /**
     *
     * @param array $params
     * @return json
     */
    public function findAll($params) {
       //dump($params);
     return $this->apiHelper->get('/annonces?' . http_build_query($params));
     
    }


}