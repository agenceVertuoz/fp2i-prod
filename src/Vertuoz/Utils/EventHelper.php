<?php

namespace Vertuoz\Utils;

class EventHelper {

    protected $apiHelper;

    /**
     *
     * @param \Vertuoz\Utils\ApiHelper $apiHelper
     */
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }

    public static function slugify($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    /**
     *
     * @param array $params
     * @return json
     */
    public function listEvents($params) {
        return $this->apiHelper->get('/events?' . http_build_query($params));
    }

    /**
     *
     * @param int $id
     * @return json
     */
    public function get($id) {
        return $this->apiHelper->get('/events/' . $id);
    }

     
}
