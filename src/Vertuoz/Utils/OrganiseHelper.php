<?php

namespace Vertuoz\Utils;

class OrganiseHelper {

    protected $apiHelper;

    /**
     * 
     * @param \Vertuoz\Utils\ApiHelper $apiHelper
     */
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }

    /**
     * 
     * @param int $id
     * @return json
     */
    public function get($id) {
        return $this->apiHelper->get('/organise/' . $id);
    }

    public function getOneByArea($code,$language) {
        $params= array('area'=> $code ,'language' => $language);
        //$menus = $this->apiHelper->get('/organise?area=' . $code );
        $menus = $this->apiHelper->get('/organise?'. http_build_query($params) );
        
        if(count($menus) > 0)
            return $menus[0];
        else
            return null ;
    }

}
