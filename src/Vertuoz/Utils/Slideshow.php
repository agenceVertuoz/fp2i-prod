<?php

namespace Vertuoz\Utils;

class Slideshow {

    /**
     * 
     * @param string $cdnUrl
     * @param int $siteId
     * @param string $imageSize
     * @param string $imageFile
     * @return string
     */
    public static function getSlidePictureFullUrl($cdnUrl, $siteId, $imageSize, $imageFile) {
        return $cdnUrl . '/documents/' . $siteId . '/slideshow3/' . $imageSize . '_' . $imageFile;
    }

}
