<?php

namespace Vertuoz\Utils;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class OpenWeatherMapHelper {

    protected $apiUrl;
    protected $data;
    protected $cacheDuration = 3600; // 1h
    protected $cacheFile = 'response.txt';
    protected $lastUpdate;

    /**
     * @param string $apiUrl
     * @param string $appId
     * @param string $location
     * @param string $cacheFile
     */
    public function __construct($apiUrl, $appId, $location, $cacheFile = __DIR__ . '/response.txt') {
        
        $this->apiUrl = $apiUrl;
        $this->appId = $appId;
        $this->cacheFile = $cacheFile;

        if (!file_exists($this->cacheFile)) {
            // Create cache file
            $this->createCache();
        }

        // Read file cache
        $cache = $this->parseCache();
        
        if (!$this->useCache($cache)) {
            // OpenWeatherMap API call
            $response = $this->getResponseFromAPI($apiUrl, $appId, $location);
            $responseJson = $response->getBody()->getContents();
            $this->storeCache($responseJson);
        }
        else {
            $responseJson = $cache['response'];
        }
        
        $this->data = $this->format($responseJson);

    }
    
    private function format($responseJson) {
        
        $arr = json_decode($responseJson, true);
        $icon = $arr['weather'][0]['icon'].'.png';
        $tempMin = self::convertKelvinToDegrees($arr['main']['temp_min']);
        $tempMax = self::convertKelvinToDegrees($arr['main']['temp_max']);
        
        return [
            "icon" => "/images/weather/" . $icon,
            "tempMin" => $tempMin,
            "tempMax" => $tempMax
        ];
        
    }
    
    private static function convertKelvinToDegrees($kelvin) {
        return $kelvin - 273.15;
    }

    public function getData() {
        return $this->data;
    }

    private function useCache($cache) {
        if (array_key_exists('date', $cache)) {
            $lastUpdated = $cache['date'];
            return time() < strtotime($lastUpdated) + $this->cacheDuration;
        }
        return false;
    }


    private function storeCache($response = "") {
        
        $data[] = "Date : ".date('Y-m-d H:i:s');
        $data[] = "------------------------------";
        $data[] = $response;
        
        $str = implode("\r\n", $data);
        $str .= "\r\n";
        
        file_put_contents($this->cacheFile, $str);

    }
    
    
    private function createCache() {
        
        $data[] = "Date : ".date('Y-m-d H:i:s', time() - $this->cacheDuration);
        $data[] = "------------------------------";
        
        $str = implode("\r\n", $data);
        $str .= "\r\n";
        
        file_put_contents($this->cacheFile, $str);
        
    }


    private function parseCache() {

        $str = file_get_contents($this->cacheFile);
        $explodedStr = explode("------------------------------", $str);
        $date = str_replace("Date : ", "", $explodedStr[0]);
        $response = $explodedStr[1];

        return [
            "date" => $date,
            "response" => $response
        ];

    }

    private function getResponseFromAPI($apiUrl, $appId, $location) {

        $client = new Client();

        try {

            $response = $client->request('GET', $apiUrl, [
                'query' => [
                    'q' => $location,
                    'appId' => $appId
                ]
            ]);
            
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            exit($e->getMessage());
        }

        return $response;
    }

}
