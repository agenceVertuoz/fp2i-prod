<?php

namespace Vertuoz\Utils;

class SliderHelper {

    protected $apiHelper;
    protected $slides;
    
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }
    
    /**
     * 
     * @param int $id
     * @return json
     */
    public function get($id,$language) {
        return $this->apiHelper->get('/slideshows/'.$id. '?language='.$language);
    }
    
    /**
     * 
     * @param type $name
     * @return json
     */
    public function getOneByName($name,$language) {
        $slider =  $this->apiHelper->get('/slideshows?name='.$name . '&language='.$language);
        if(count($slider) > 0)
        {
            return $slider[0];
        }
        return null;
    }
    

}
