<?php

namespace Vertuoz\Utils;

class ExtrafieldHelper {

    protected $apiHelper;

    /**
     * 
     * @param \Vertuoz\Utils\ApiHelper $apiHelper
     */
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }

    /**
     * 
     * @param array $params
     * @return json
     */
    public function listExtrafields($params) {
        return $this->apiHelper->get('/extrafields?' . http_build_query($params));
    }

    /**
     * 
     * @param int $index
     * @return json
     */
    public function getByIndex($index) {
        return $this->apiHelper->get('/extrafields/' . $index);
    }

}
