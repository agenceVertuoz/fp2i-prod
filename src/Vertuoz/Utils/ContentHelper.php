<?php

namespace Vertuoz\Utils;

class ContentHelper {

    protected $apiHelper;

    /**
     *
     * @param \Vertuoz\Utils\ApiHelper $apiHelper
     */
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }

    public static function slugify($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    /**
     *
     * @param array $params
     * @return json
     */
    public function listContents($params) {
        
        return $this->apiHelper->get('/contents?' . http_build_query($params));
    }

    /**
     *
     * @param int $id
     * @return json
     */
    public function get($id) {
        return $this->apiHelper->get('/contents/' . $id);
    }

    public function getOneByCode($code, $language) {
        $contents = $this->apiHelper->get('/contents?code=' . $code . '&language=' . $language);
        if (count($contents) > 0) {

            return $contents[0];
        }
        return null;
    }

//    public function getOneByPageUrl($url) {
//        $contents = $this->apiHelper->get('/contents?pageUrl=' . $url);
//        if (count($contents) > 0) {
//
//            return $contents[0];
//        }
//        return null;
//    }

    public function getOneByReadMoreLink($url) {

        $contents = $this->apiHelper->get('/contents?readMoreLink=/' . $url);
        if (count($contents) > 0) {

            return $contents[0];
        }
        return null;
    }

    public function getOneByReadMoreLinkAndAreaNameNotIn($url,$language, $excludes)
    {
        $contents = $this->apiHelper->get('/contents?readMoreLink=/' . $url . '&language=' . $language);
        foreach ($contents as $content) {
            if (!in_array($content['areaName'], $excludes)) {
                return $content;
            }
        }
        return null;
    }

    public function getAllByArea($code) {
        $contents = $this->apiHelper->get('/contents?order=date&direction=DESC&areaName=' . $code);

        return $contents;
    }

}
