<?php

namespace Vertuoz\Utils;

class ImmocriteriaHelper {

    protected $apiHelper;

    /**
     * 
     * @param \Vertuoz\Utils\ApiHelper $apiHelper
     */
    public function __construct(\Vertuoz\Utils\ApiHelper $apiHelper) {
        $this->apiHelper = $apiHelper;
    }
    
    /**
     *
     * @param array $params
     * @return json
     */
    public function findAll($params) {
        //dump($params);
     return $this->apiHelper->get('/immocriteria?'. http_build_query($params));
     
    }


}